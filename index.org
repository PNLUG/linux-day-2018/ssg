# -*- org-reveal-title-slide: "title-slide.html" -*-
# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org
#+TITLE: Static Website Generators
#+SUBTITLE: Linuxday 2018
#+AUTHOR: Stefano Morandi
#+DATE: 2018-10-27

* Intro

- HTTP
- HTML / CSS
- SHELL / GIT
- WEB SERVICES


* Internet today

** cms and programming lang.

Wordpress: 32%

PHP: 42%

** LAMP

- Linux ;-)
- Apache
- PHP
- MySQL

#+ATTR_REVEAL: :frag (appear)
not so easy...

** Scenario: WP simple

[[./img/wp-like-simple.png]]

** Scenario: WP big

[[./img/wp-like-scale.png]]

#+BEGIN_NOTES
Come Windows: va mantenuto
- estensioni
- temi
- core
#+END_NOTES

* SSG

Static Site Generators

** 2019

[[./img/giphy.gif]]

** definition

#+BEGIN_QUOTE
a static site generator takes your site content, applies it to templates, and generates a structure of purely static HTML files
#+END_QUOTE

#+BEGIN_NOTES
DYN: renderizzare la pagina ogni volta => cache nightmare
SSG: renderizzo una volta sola, sul mio computer

Sposto il carico dal momento della visita a quello della generazione del contenuto
#+END_NOTES

** definition

[[./img/ssg.png]]

** Markdown

#+BEGIN_QUOTE
Markdown is a lightweight markup language with plain text formatting syntax
#+END_QUOTE

** Markdown

#+BEGIN_SRC markdown
# Heading

## Sub-heading

Paragraphs are separated
by a blank line.

Two spaces at the end of a line
produces a line break.

Text attributes _italic_,
**bold**, `monospace`.
#+END_SRC

** Why

- Secure
- Fast
- Cheap

#+BEGIN_NOTES
sicuro: non c'e' codice :-)
quello che non c'e' non si rompe

veloce: tutto statico => tutto in cache
economico: AWS centesimi al mese
#+END_NOTES

** When

- Blog
- Landing pages
- Information
- No Sharing
- No Users

#+BEGIN_NOTES
- Non c'e' la gestione utenti (BENE! GDPR!)
- Form
- Campagna media su sito Magento/WP
#+END_NOTES

** Scenario: SSG

[[./img/ssg-like.png]]

#+BEGIN_NOTES
Sto imbrogliando :-D

Non è mai così semplice, ci sono i webservice di cui tenere conto
#+END_NOTES

** Scenario: WP like

[[./img/wp-like-simple.png]]

** Web services

- Disqus
- Formspree
- Sitesearch 360
- Snipcart

** Static Site Generators

- Jekyll (ruby)
- Hexo (js)
- Hugo (go)

[[https://www.staticgen.com/][List of Static Site Generators]]

** Static CMS

- https://getpublii.com/
- https://www.netlifycms.org/


* HUGO

** hugo

[[./img/hugo-logo.png]]

The world’s fastest framework for building websites

** features

- easy to install and cross platform
- markdown or emacs orgmode :-D
- mutilingual
- output JSON

** themes

[[./img/hugo-themes.png]]


* Live session


#+MACRO: copyrightyears 2017
#+INCLUDE: license-template.org
